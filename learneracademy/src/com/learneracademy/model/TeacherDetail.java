package com.learneracademy.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Teacher")
public class TeacherDetail implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "TEACHER_ID")
	private int teacherID;

	@Column(name = "TEACHER_NAME")
	private String teacherName;

	@Column(name = "TEACHER_CONTACT")
	private long teacherContact;
	
//	//Successful 
//	@ManyToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name = "CLASS_ID")
//	private ClassDetail teacherdetail;
	
//	//success Main flow ( Teacher Mapped to a class)
//	//One teacher can go to many classes
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "classdetail")
//	private Set<ClassDetail> classdetail = new HashSet<ClassDetail>();

	
	// Successfull- many subjects can be covered by one teacher
	//One teacher can deal  many subjects
	
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "teachersubjectdetail")
	private Set<SubjectDetail> teachers ;
	//= new HashSet<SubjectDetail>();
	

	public TeacherDetail() {
		// TODO Auto-generated constructor stub
	}




//	public Set<ClassDetail> getClassdetail() {
//		return classdetail;
//	}
//
//
//
//
//	public void setClassdetail(Set<ClassDetail> classdetail) {
//		this.classdetail = classdetail;
//	}




	


	public int getTeacherID() {
		return teacherID;
	}

	


	public Set<SubjectDetail> getTeachers() {
		return teachers;
	}




	public void setTeachers(Set<SubjectDetail> teachers) {
		this.teachers = teachers;
	}




	public void setTeacherID(int teacherID) {
		this.teacherID = teacherID;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public long getTeacherContact() {
		return teacherContact;
	}

	public void setTeacherContact(long teacherContact) {
		this.teacherContact = teacherContact;
	}

	public TeacherDetail(String teacherName, long teacherContact) {
		super();
		this.teacherName = teacherName;
		this.teacherContact = teacherContact;
	}

}
