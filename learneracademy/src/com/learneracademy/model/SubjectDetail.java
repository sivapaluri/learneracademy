package com.learneracademy.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Subject")

public class SubjectDetail implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "SUB_SERIAL_NUMBER")
	private int sNo;

	@Column(name = "SUBJECT_CODE")
	private String subjectCode;
	@Column(name = "SUBJECT_NAME")
	private String subjectName;
	
	//Success Full-many subjects can be covered by one teacher
	@ManyToOne
	@JoinColumn(name = "TEACHER_ID")
	private TeacherDetail teachersubjectdetail;
	
//	//One class can have many subjects
//	//many subjects can be covered in one class
//	@ManyToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name = "CLASS_ID")
//	private ClassDetail classsubjectdetail;

	

	public SubjectDetail() {
		// TODO Auto-generated constructor stub
	}
	
//	public ClassDetail getClasssubjectdetail() {
//		return classsubjectdetail;
//	}
//
//	public void setClasssubjectdetail(ClassDetail classsubjectdetail) {
//		this.classsubjectdetail = classsubjectdetail;
//	}

	
	public TeacherDetail getTeachersubjectdetail() {
		return teachersubjectdetail;
	}

	public void setTeachersubjectdetail(TeacherDetail teachersubjectdetail) {
		this.teachersubjectdetail = teachersubjectdetail;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public int getsNo() {
		return sNo;
	}

	public void setsNo(int sNo) {
		this.sNo = sNo;
	}

	public SubjectDetail(String subjectCode, String subjectName) {
		super();
		this.subjectCode = subjectCode;
		this.subjectName = subjectName;
	}

}
