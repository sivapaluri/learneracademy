package com.learneracademy.model;

import java.util.HashSet;


import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Class")
public class ClassDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CLASS_ID")
	private int id;

	@Column(name = "CLASS_NAME")
	private String className;

//Successful 
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "teacherdetail")
//	private Set<TeacherDetail> teacherdetail = new HashSet<TeacherDetail>();
//	
//	// success main flow
//	// Many teachers can cometo one class
//	@ManyToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name = "TEACHER_ID")
//	private TeacherDetail classdetail;

	// many subjects can be covered in one class
	// One subject can be covered in many classes

	// One class can have many subjects
	
//	@JsonIgnore
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "classsubjectdetail")
//	private Set<SubjectDetail> classsubjectdetail = new HashSet<SubjectDetail>();

	public ClassDetail() {

	}

//	public TeacherDetail getClassdetail() {
//		return classdetail;
//	}
//
//	public void setClassdetail(TeacherDetail classdetail) {
//		this.classdetail = classdetail;
//	}

	public int getid() {
		return id;
	}

//	public Set<SubjectDetail> getClasssubjectdetail() {
//		return classsubjectdetail;
//	}
//
//	public void setClasssubjectdetail(Set<SubjectDetail> classsubjectdetail) {
//		this.classsubjectdetail = classsubjectdetail;
//	}

	public void setid(int id) {
		this.id = id;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public ClassDetail(String className) {
		super();
		this.className = className;
	}

}