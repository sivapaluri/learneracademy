package com.learneracademy.Service;

import java.util.List;

import com.learneracademy.model.StudentDetail;

public interface StudentDetailService {

	public StudentDetail createStudentdetail(StudentDetail studentdetail);

	public List<StudentDetail> getStudentDetails();

	public StudentDetail updateStudentDetail(StudentDetail studentdetail);

	public StudentDetail getStudentDetailById(int studentRoll);

	public void removeStudentDetail(int studentRoll);

}
